// AJOUT INFOLETTRE 

const bouton = document.getElementById('btn_infolettre');
const infolettre = document.getElementById('infolettre');
const fermer = document.getElementById('fermer');
const lesBoutons = document.getElementById('lesBoutons');

bouton.addEventListener('click', ouvrir_infolettre)
fermer.addEventListener('click', fermer_infolettre)

function ouvrir_infolettre(e){
  infolettre.style.display = "block";
  bouton.style.display = "none";
}

function fermer_infolettre(){
  infolettre.style.display = "none";
}
