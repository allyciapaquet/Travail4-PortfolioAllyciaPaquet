
let error = [];
const prenom = document.getElementById("prenom");
const nom = document.getElementById("nom");
const email = document.getElementById("courriel");
const telephone = document.getElementById("telephone")
const comment = document.getElementById("commentaire");
const utilisateur = document.getElementById("utilisateur");


const captcha = document.getElementById('captcha');

const champs = [prenom.value, nom.value, email.value, telephone.value, comment.value, utilisateur.value, captcha.value];

const formulaire = document.getElementById('form')
formulaire.addEventListener('submit', validerChamps)

// Outils de comparaison
const courrielRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const telephoneRegex = /\+?([\d|\(][\h|\(\d{3}\)|\.|\-|\d]{4,}\d)/

function validerChamps(e){
  const messages = []

  // Pour savoir s'il y a un caractères quelconque dans les input
  if (prenom.value.length <= 0) {
    messages.push("Le champs prénom n'est pas remplis")
  }
  if (nom.value.length <= 0) {
      messages.push("Le champs nom n'est pas remplis")
  }
  if (courriel.value.length <= 0) {
    messages.push("Le champs courriel n'est pas remplis")
  }

  if (telephone.value.length <= 0) {
    messages.push("Le champs téléphone n'est pas remplis")
  }

  if (utilisateur.value.length <= 0) {
    messages.push("Le champs utilisateur n'est pas remplis")
  }

  if (comment.value.length <= 0) {
    messages.push('Le champs de votre texte doit contenir des caractères')
  }

  if(courrielRegex.test(email.value) === false){
    messages.push("Le courriel n'est pas valide")
  }

  if(telephoneRegex.test(telephone.value) === false){
    messages.push("Le téléphone n'est pas valide")
  }

  if (messages.length > 0) {
      e.preventDefault()
      const ul = document.getElementById('messages')
      ul.innerHTML = ""
      for(let i = 0; i < messages.length; i++){
          const li = document.createElement('li')
          const texte = document.createTextNode(messages[i])
          li.appendChild(texte)
          ul.appendChild(li)
          li.classList.add('transparent')
      }
      
  }  
}
